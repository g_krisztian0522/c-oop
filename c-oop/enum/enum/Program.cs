﻿using System;

namespace @enum
{
    enum napok
    {
        Hétfő = 1,
        Kedd = 2,
        Szerda = 3,
        Csütörtök = 4,
        Péntek = 5,
        Szombat = 6,
        Vasárnap = 7
    }
    class Program
    {
        static void Main(string[] args)
        {
            //Az enum x elemének elérése:
            /* 1. Számmal index(?)-szel*/
            Console.WriteLine(Enum.GetName(typeof(napok), 2));
            /* 2. Értékkel */
            Console.WriteLine(napok.Kedd);
            /* Hivatkozás értékkel: Enum.Érték */
            Console.WriteLine("A hét első napja: {0}\nA hét utolsó napja: {1}", napok.Hétfő, napok.Vasárnap);

            foreach (var elem in Enum.GetNames(typeof(napok))) // Enum iterálása, az adott elem értékét írja ki
            {
                Console.WriteLine(elem);
            }

            Console.ReadKey();
        }
    }
}
